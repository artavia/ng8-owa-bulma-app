# Description
Fun with Angular v8, Bulma and OpenWeatherMap.

## Building on previous precedent.
Other than putting out to pasture some really old css, Bulma has been brought in to rescue the project. Plus, it was built with **@angular/cli@8.3.0** because in my experience **@angular/cli@8.3.2** breaks (and, I do not have the time address this or investigate further). I had occasion to use Directive, too, in order to access html5 custom data attributes. 

## What is presently implemented
I have added the custom error and loading elements. I experimented with an Interceptor but I may waste it altogether and is yet to be seen. Finally, I implemented some customized animations with page and/or component transitions. The transitions are used to accentuate certain portions of the project; they&rsquo;re not included to distract from the rest of the presentation. **Are you following me?** Take a guitar virtuoso like Jimi Hendrix: he did not use the wah&#45;wah effect in every other measure to make the point. Instead, the wah&#45;wah effect was used sparingly and to make his signature sound more unique. The musical arrangement was more important than silly effects which added color and texture to the overall presentation!

## What is not implemented
I may yet eke out a "shared" form. But, I have bigger fish to fry at the moment.

## Please visit new project today
You can see [the lesson at surge.sh](https://ng8-owa-bulma-app.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## There is an available companion project
You can see the [original repository](https://gitlab.com/artavia/cra3-owa-bulma-app "the lesson at gitlab"), then, decide if that is something for you to play around with, too.

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!