import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { 
  HttpClientModule
  , HTTP_INTERCEPTORS 
} from '@angular/common/http';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule , routedComponents } from './app-routing.module';

import { AppComponent } from './app.component';
import { HttpErrorInterceptor } from './http-error-interceptor';
import { BurgerDirective } from './directives/burger.directive';
import { MyPresentYearDirective } from './directives/my-present-year.directive';

@NgModule({
  declarations: [
    AppComponent
    , routedComponents
    , BurgerDirective
    , MyPresentYearDirective
  ],
  imports: [
    BrowserModule
    , BrowserAnimationsModule
    , AppRoutingModule
    , FormsModule
    , HttpClientModule
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS
      , useClass: HttpErrorInterceptor
      , multi: true 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
