import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeoobservableService {

  constructor() { }

  // experiment 1 - getCurrentPosition
  locationObservable = new Observable( (observer) => {
    
    if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition( 
        (pos) => {
          // console.log( 'pos' , pos );
          observer.next(pos);
          observer.complete();
          return observer.unsubscribe();
        }
        , ( err ) => {
          // console.log( 'Geo observable service says: Position is not available' );
          // observer.error( 'Geo observable service says: Position is not available' );
          
          // console.log( 'Geo observable service... err:' , err );
          return observer.error( err );
          
        }
        , { enableHighAccuracy: true }
      );
    }
    else {
      console.log( 'Geo observable service says: geolocation not supported' );
      observer.error( 'Geo observable service says: geolocation not supported' );
    }

  } );
}
