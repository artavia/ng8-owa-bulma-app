import { Observable, of } from 'rxjs';

import { Injectable } from '@angular/core';
import { Location, locations } from '../classes/data-model';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor() { }

  // async version
  getLocations(): Observable<Location[]>{ 
    return of( locations ); 
  }
  
}
