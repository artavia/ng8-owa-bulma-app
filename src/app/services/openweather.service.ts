import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { OwaApiKeyService } from './owa-api-key.service';

@Injectable({
  providedIn: 'root'
})
export class OpenweatherService {

  private baseUrl = `https://api.openweathermap.org`;
  private weatherpath = `/data/2.5/weather`;
  private forecastpath = `/data/2.5/forecast`;
  private appId = this.apiKeySvc.apiKey;
  private unitTemp = `imperial`; // e.g. -- metric, imperial
  private linguafranca = `en`; // e.g. -- en, es, ja
  private query = `appid=${this.appId}&units=${this.unitTemp}&lang=${this.linguafranca}`;

  constructor( private apiKeySvc: OwaApiKeyService , private http: HttpClient ) { }

  // ################# weather reporting on 1 day #################
  
  // By city ID 
    // http://api.openweathermap.org/data/2.5/weather?id={id}

    selectWeatherData(selVal: number):Observable<{}>{
      const urlstring = `${this.baseUrl}${this.weatherpath}?id=${selVal}&${this.query}`;
      return this.http.get( urlstring );
    }
  
    // By geographic coordinates
      // http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}
  
    geoWeatherData(lat: number, lon: number ):Observable<{}>{
      const urlstring = `${this.baseUrl}${this.weatherpath}?lat=${lat}&lon=${lon}&${this.query}`;
      return this.http.get( urlstring );
    }
  
    // ################# forecast reporting on 5 days #################
  
    // By city ID 
      // http://api.openweathermap.org/data/2.5/forecast?id={id}
  
    selectForecastData(selVal: number):Observable<any>{
      const urlstring = `${this.baseUrl}${this.forecastpath}?id=${selVal}&${this.query}`;
      return this.http.get( urlstring );
    }
  
    // By geographic coordinates
      // http://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}
  
    geoForecastData(lat: number, lon: number ):Observable<any>{
      const urlstring = `${this.baseUrl}${this.forecastpath}?lat=${lat}&lon=${lon}&${this.query}`;
      return this.http.get( urlstring );
    }

}
