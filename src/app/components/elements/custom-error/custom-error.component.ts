import { Component, OnInit, Input } from '@angular/core';

import { fadeInAnimation } from '../../../custom-animations/fade-in.animation';

@Component({
  selector: 'app-custom-error',
  templateUrl: './custom-error.component.html',
  styleUrls: ['./custom-error.component.scss']

  // make fade in animation available to this component
  , animations: [ fadeInAnimation ]

  // attach the fade in animation to the host (root) element of this component
  , host: { '[@fadeInAnimation]' : '' } 
  
})
export class CustomErrorComponent implements OnInit {

  @Input('isError') isError;

  constructor() { }

  ngOnInit() {
  }

}
