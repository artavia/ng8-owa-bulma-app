import { Observable, Subscription, from, throwError as _throw } from 'rxjs';

import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { fadeInAnimation } from '../../../custom-animations/fade-in.animation';

import { Location } from '../../../classes/data-model';

import { CityService } from '../../../services/city.service';
import { OpenweatherService } from '../../../services/openweather.service';
import { GeoobservableService } from '../../../services/geoobservable.service';


@Component({
  selector: 'app-form-forecast',
  templateUrl: './form-forecast.component.html',
  styleUrls: ['./form-forecast.component.scss']

  // make fade in animation available to this component
  , animations: [ fadeInAnimation ]

  // attach the fade in animation to the host (root) element of this component
  , host: { '[@fadeInAnimation]' : '' } 
  
})
export class FormForecastComponent implements OnInit {

  locations: Location[]; // versions 1 and 2
  selectedValue = null;
  myChosenValue = null;
  selectedWeather= null;
  selectedCityList = null;

  isLoading: boolean = false;
  isError: any = null;

  // jolo stuff, too
  currentLat: any;
  currentLong: any;
  singleObservable$: Observable<{}>;
  singleSubscription: Subscription;
  boundSinglePosition: any;

  constructor( private citysvc: CityService , private openweathersvc: OpenweatherService, public geosvc: GeoobservableService ) {
  }

  ngOnInit(){
    this.getLocations(); 
  }

  // async version
  getLocations(): void { 
    this.citysvc.getLocations().subscribe( ( locations ) => {
      this.locations = locations;
    } );
  }

  onForecastChange( newValue:any ): void {

    this.isLoading = true;
    this.isError = null;

    // console.log( 'newValue' , newValue );
    this.selectedValue = newValue;
    this.ifNotNullForecast( this.selectedValue );
  }

  ifNotNullForecast( pm ){
    if( pm !== null ){
      const myValue = pm.name;
      this.myChosenValue = myValue;
      this.runChangeForecast( pm.id );
    }
    else
    if( pm === null ){
      this.isLoading = false;

      this.selectedWeather = null;
    }
  }

  runChangeForecast( id: number ){
    this.openweathersvc.selectForecastData( id ).subscribe( 
      
      ( data ) => { 

        this.selectedCityList = data.list.filter( function(el) {
          return ( el.dt_txt.includes( '12:00:00' ) );    // for normal browsers
          // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
        } ); // console.log( 'this.selectedCityList' , this.selectedCityList );

        this.selectedWeather = data; 
        // this.initFormattingMultipleDates( this.selectedWeather );
      } 
      
      , ( err: HttpErrorResponse ) => {

        this.isLoading = false;

        // console.log( `le HttpErrorResponse - runChangeForecast: ${err}` );
        // console.log( "err: ", err );
        this.isError = err;

        // return Observable.throw(err); // EXTRA!

      } 

    );
  } 

  onForecastClick( e ):void {
    this.isLoading = true;
    this.isError = null;

    this.currentPositionForecast();
  }

  currentPositionForecast() {
    
    this.singleObservable$ = this.geosvc.locationObservable;

    const fromObs$ = from( this.singleObservable$ );
    this.singleSubscription = fromObs$.subscribe(
      (pos) => {

        this.isLoading = false;

        // console.log( 'pos' , pos );
        this.boundSinglePosition = pos;
        this.currentLat = this.boundSinglePosition.coords.latitude;
        this.currentLong = this.boundSinglePosition.coords.longitude;
        this.runGeoForecast( this.currentLat , this.currentLong );
      }
      , ( err: HttpErrorResponse ) => {
        
        this.isLoading = false;

        this.isError = err;

        // console.warn( `le HttpErrorResponse - currentPositionForecast: ${err}` );
        return Observable.throw(err);
      }
      , () => {
        // console.log( 'Your query has been completed' );
      }
    );
  } 

  runGeoForecast( lat: number, lon: number ){ 
    this.openweathersvc.geoForecastData( lat, lon ).subscribe( 
      
      ( data ) => { 

        this.isLoading = false;

        this.selectedCityList = data.list.filter( function(el) {
          return ( el.dt_txt.includes( '12:00:00' ) );    // for normal browsers
          // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
        } ); // console.log( 'this.selectedCityList' , this.selectedCityList );
        this.selectedWeather = data; 
        // this.initFormattingMultipleDates( this.selectedWeather );
      } 
      
      , ( err: HttpErrorResponse ) => {

        this.isLoading = false;
        
        // console.log( `le HttpErrorResponse - runGeoForecast: ${err}` );
        // console.log( "err: ", err );

        this.isError = err;
      } 
      
    );
  }

}
