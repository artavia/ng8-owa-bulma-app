import { Component, OnInit, ViewChild } from '@angular/core';
import { BurgerDirective } from '../../../directives/burger.directive';

import { MyPresentYearDirective } from "../../../directives/my-present-year.directive";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  @ViewChild( BurgerDirective , { static: true } ) burgerEl: BurgerDirective;
  @ViewChild( MyPresentYearDirective , { static: true } ) hotelThreeEl: MyPresentYearDirective;

  public bulmaLogoUrl = "/assets/images/layout/bulma-logo.png";  

  constructor() { }

  ngOnInit() {
    this.hotelThreeEl.printYear();
  }

  noOpLink(e: Event): void {
    e.preventDefault();
  }

  flexExtendBurger(): void {
    this.burgerEl.flexExtend();
  }

}
