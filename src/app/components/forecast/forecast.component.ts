import { Component, OnInit, Input } from '@angular/core';
import { DatePipe, DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {

  public logoUrl = "/assets/images/layout/owa_logo.gif";
  
  @Input('isError') isError;
  @Input('isLoading') isLoading;

  @Input('selectedWeather') selectedWeather;
  @Input('selectedCityList') selectedCityList;

  constructor() { }

  ngOnInit() {
    // console.log( 'this.selectedWeather' , this.selectedWeather ); // null
  }

}
