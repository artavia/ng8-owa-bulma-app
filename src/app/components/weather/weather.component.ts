import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  public logoUrl = "/assets/images/layout/owa_logo.gif";

  @Input('isError') isError;
  @Input('isLoading') isLoading;

  @Input('selectedWeather') selectedWeather;
  @Input('formatted_currentdate') formatted_currentdate;
  @Input('formatted_sunrise') formatted_sunrise;
  @Input('formatted_sunset') formatted_sunset;
  @Input('formatted_dayofweek') formatted_dayofweek;

  constructor() { }

  ngOnInit() {
    // console.log( 'this.selectedWeather' , this.selectedWeather ); // null
  }

}
