import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormWeatherComponent } from './components/elements/form-weather/form-weather.component';
import { FormForecastComponent } from './components/elements/form-forecast/form-forecast.component';


import { WeatherComponent } from './components/weather/weather.component';
import { ForecastComponent } from './components/forecast/forecast.component';
import { NavComponent } from './components/elements/nav/nav.component';
import { FooterComponent } from './components/elements/footer/footer.component';
import { LogoComponent } from './components/elements/logo/logo.component';
import { LoadingComponent } from './components/elements/loading/loading.component';
import { CustomErrorComponent } from './components/elements/custom-error/custom-error.component';


const routes: Routes = [
  { path: 'weather' , component: FormWeatherComponent }
  , { path: 'forecast' , component: FormForecastComponent }
  , { path: '' , redirectTo: '/weather', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [ FormWeatherComponent, FormForecastComponent, WeatherComponent, ForecastComponent, NavComponent, FooterComponent, LogoComponent, LoadingComponent, CustomErrorComponent ];