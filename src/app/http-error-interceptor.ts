import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError as _throw, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


export class HttpErrorInterceptor implements HttpInterceptor {

  intercept( request: HttpRequest<any>, next: HttpHandler ) : Observable<HttpEvent <any>> {

    return next.handle(request).pipe(
      retry(1) 
      , catchError( this.processError ) 
    );

  };

  private processError( error: HttpErrorResponse ): Observable<any> {

    /*  let errorMessage = ''; 

    if ( error.error instanceof ErrorEvent) { // client-side error
      errorMessage = `le client-side Error: ${error.error.message}`;
    }
    else { // server-side error
      errorMessage = `le server-side Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    console.log( "le errorMessage: " , errorMessage ); */

    if( error.error instanceof Error){
      console.log( "Error... error: ", error );
    }
    else
    if ( error.error instanceof ErrorEvent){
      console.log( "ErrorEvent... error: ", error );
    }
    else
    if ( error.error instanceof HttpErrorResponse ){
      console.log( "HttpErrorResponse... error: ", error );
    }
    
    return throwError( error );
    
  }

};