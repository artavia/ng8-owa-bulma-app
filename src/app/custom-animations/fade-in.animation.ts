import {
  trigger
  , animate
  , transition
  , style
} from '@angular/animations';

// trigger name for attaching this animation to an element using the [@triggerName] syntax

export const fadeInAnimation = trigger( 'fadeInAnimation' , [
  
  // route 'enter' transition
  transition( ':enter' , [
    
    style( { opacity: 0 } ) // css styles at start of transition

    , animate( '.365s' , style( { opacity: 1 } ) ) // animation and styles at end of transition

  ] )

] );

