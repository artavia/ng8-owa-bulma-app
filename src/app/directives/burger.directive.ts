import { Directive, AfterViewInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[appBurger]'
})
export class BurgerDirective implements AfterViewInit {

  mytarget: string;

  constructor( private elem: ElementRef ) {
    //
  }

  ngAfterViewInit(){
    // console.log( "this.elem.nativeElement: " , this.elem.nativeElement );

    // this.mytarget = this.elem.nativeElement.getAttribute('data-mytarget');
    this.mytarget = this.elem.nativeElement.dataset.mytarget;  // console.log( "this.mytarget: " , this.mytarget ); 
  }

  flexExtend(){
    let burger = this.elem.nativeElement;  // console.log( 'burger: ' , burger );
    const attrTarget = this.mytarget;  // console.log( "attrTarget: " , attrTarget ); 
    const $targetEl = document.querySelector(`#${attrTarget}`);  // console.log( "$targetEl: " , $targetEl );
    burger.classList.toggle('is-active');
    $targetEl.classList.toggle('is-active');
  }

}
