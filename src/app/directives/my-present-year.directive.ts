import { Directive , ElementRef } from '@angular/core';

@Directive({
  selector: '[appMyPresentYear]'
})
export class MyPresentYearDirective {

  constructor( private elem: ElementRef ) { }
  
  printYear(): void{
    
    // console.log( "this.elem.nativeElement: " , this.elem.nativeElement );

    let newDate = new Date();
    let year = newDate.getFullYear();
    let yearstring = year.toString(); 
    let textnode = document.createTextNode(` ${yearstring}`);
    this.elem.nativeElement.appendChild( textnode );

  }

}
